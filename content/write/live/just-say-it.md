+++
author = "zj"
title = "说说吧"
date = "2024-05-15"
description = "最近状态"
tags = [
    "live",
]
categories = [
    "live",
]
draft = true
+++

1. 这几天状态不太好。酒喝的不受自己控制，觉睡的总做梦，饭吃的也不香，因此说说吧。
2. 下班后，没有事情做的时候，一股子无所事事的感觉，像一只茫然的羔羊，履步在街道上，于是这个时候酒神便来唤我。喝着喝着心中多了些舒坦，最难控制的便是此刻。再喝点更舒坦，再喝点便愉悦，再喝点便失去自我，再喝点便断片，再喝点第二天谁难受谁知道了。喝的太多了，第二天一定难受，轻微的就是不想吃饭，肚子受不了，严重点的就是会吐，整天整天的围绕着想吐的难受当中，到了晚上会好点，可以吃点东西。
3. 喝酒这件事情带了的好处，消解茫然，促使内心平静。其次，鼓舞内心，充满希望。喝酒带来的坏处，肚子、肝脏、心脏、血液，虽然表面只是食欲不振，如果长此以往，也是在加快缩短它们存在的时间。
4. 夜深的时候，不想去睡觉。放不下手中的事情，刷抖音、刷微博、下象棋。似乎一停下来，脑子中就会涌现出一些，无法想明白，无法面对，无法释怀的事情。谓之，虚无之神来唤我，像极了那首歌里唱的，```Hello darkness, my old friend```。一件简单的事情，与同事的某句对话是否合理，会在脑子中想过八百种可能的结果。战败的时候便归于虚无之神麾下，认为一切没有意义，继而低迷不振。这种时刻，大部分是精神损耗，其次是器官损耗。也不是次次战败，胜利的时候，会归于其他精神指导中，罗曼罗兰的```得对着这新来的日子抱着虔敬的心。别想什么一年十年以后的事。你得想到今天。把你的理论统统丢开。所有的理论，哪怕是关于道德的，都是不好的，愚蠢的，对人有害的...```，加缪的```去用残损的手掌抚平彼此的创痕固执地走向幸福人只要竭尽全力就应该是幸福的...```
5. 虚无呼唤这件事情，非常难以描述。这边依旧按照酒神那件事情来说一下它的好处与坏处。坏处是显而易见的，精神上会抑郁、焦虑、狂躁等。他有什么好处呢？我也没有办法清晰的去描述，只是想起些话来，杀不死你的终将让你强大；经历便是一种财富；你要我毁灭，我偏不。
6. 饭吃不香的事情就不说了，主要是讲了喝酒和睡觉的事情。说到这里也说完了。