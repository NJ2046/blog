+++
author = "zj"
title = "最近的生活"
date = "2023-05-11"
description = "突发的、心血来潮的"
tags = [
    "live",
]
categories = [
    "live",
]
draft = true
+++

> 當你已經唔可以再擁有嘅時候，
> 你唯一可以做嘅就喺盡量令自己唔好忘記
>
> --- [美孚新村上春樹](https://www.youtube.com/watch?v=nH-KU4xaBxw)

<audio controls>
  <source src="https://write.4587.fun/static/audios/The Lee's - 美孚新村上春树.mp3" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>


思维和笔尖的融合，想到什么就书写出什么，是很久以前的事情了。废话连篇的日子，一去不返。
### 大回顾
只是在这里敲击键盘，连接到上次在这里敲击键盘。那个时候在洛阳，在某个半弧形状的图书馆里，美女多如云，坐在那里，用键盘敲击着代码。那个时候，在这里写字的目的只有一个，便是牛逼。只是记得在[turingxi](https://blog.turingxi.top/)里想做一个[大回顾](https://blog.turingxi.top/2017/01/25/helloworld/)，写了一半，便写不下去了，种种机缘际会。turing是图灵，xi是外公的小名，叫做喜，不知道英文怎么去翻译这个字，似乎是没有的，喜是一种祝愿、态度，活得像一个吉祥物的外公。挚爱的人接二连三的离开，发现，生活本没有意义，生命是一场体验。只有当一个人从你的生活中消逝的时候，才会明白，这个人对于你的意义。有些话题太沉重了，没有办法控制泪水，因此等契机来回顾了。也没有什么大回顾了，是想说2018年到2023年之间的事情，已经说完了，也没什么非说的必要。
### 食色
爱吃、爱喝、爱女人。
### 就这样
...