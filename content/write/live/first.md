+++
author = "Hugo Authors"
title = "First"
date = "2023-04-27"
description = "Sample article showcasing basic Markdown syntax and formatting for HTML elements."
tags = [
    "live",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
+++

尝试在这里发布一篇文字。

### 一瓶汽水和一碗面条
<img  width="240" height="320" src="https://write.4587.fun/static/images/bby.jpeg">

### 一首向海洋前进歌曲
<audio controls>
  <source src="https://write.4587.fun/static/audios/hy.mp3" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

### 一段娃娃机视频
<video controls="controls" width="320" height="240" 
       name="Video Name" src="https://write.4587.fun/static/vedios/zww.mov"></video>

### 初步想法和期待结果
用手机写一篇文字并发布到博客，在手机上使用Gitlab Web IDE写字不舒服，寻找手机舒服的写字软件。看见了许久没有看见的Notion，尝试用它来做这件事情。当你在 [First Blog](https://write.4587.fun/blog/first/) 看到这些文字，证明可以在手机上完成写字和发布。

### 过程记录
午夜12点左右，用[Notion](https://www.notion.so/)在手机上写字，写好了一篇后，尝试导出为markdown，失败了。于是放弃了它与[Gitlab Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)的协作。[Notion First Word](https://glistening-hurricane-010.notion.site/8b8ef6de13f646bcb2204df322dd5bef?v=105aca6dc400496fa36dae06d05a57e9)。凌晨2点左右，在手机上使用Gitlab Web IDE和IOS的备忘录完成写字。

### 最后记录
1. Notion在手机上写字很方便，支持多种格式和媒介。
2. Gitlab Web IDE在手机上写字不方便，上传素材方便。
3. 期待Notion和Gitlab Web IDE的协作。