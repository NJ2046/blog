+++
author = "NJ"
title = "音乐"
date = "2023-08-19"
tags = [
    "music",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
+++

> 音乐表达情绪

> 音乐承载更多

> --- 吃早餐的路上脑中闪过

<audio controls autoplay loop>
  <source src="https://write.4587.fun/static/audios/北欧是我们的死亡终站.mp3" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

#### 北欧是我们的快乐老家
<img src="https://write.4587.fun/static/images/rltpt.png">
