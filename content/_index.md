---
title: "Home"
menu: "main"
weight: 1
---



Live has no meaning.
Life is an experience.



> I thought that if I could live my life again, maybe things would be different. 
> But as it turns out, everything is pretty much the same, nothing has changed. 
> I just suddenly realized that there really isn't much point in living my life again.
>
> --- [Yi_Yi](https://en.wikipedia.org/wiki/Yi_Yi)



Let's go find some fun.
